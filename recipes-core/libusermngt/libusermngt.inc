SUMMARY = "The module libusermngt allows fast access to the user datamodel by means of a temporary file."
DESCRIPTION = "When loading the library, the file is mmapped and a the modification time of the file is stored. When the library needs to access the file, it first does a stat to check if it has been updated, recreate the mapping if so, then used the mmapped memory to access the data.  This initial mmap and the final munmap are done using constructor and destructor functions.  To avoid having all the PCB plugins linking against SSL, the function to authenticate users is provided in its own library. SSL is needed there to calculate the SHA-256 checksum of the salt and passwor."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

DEPENDS += "openssl libsahtrace"

CONFIG_SAH_LIB_USERMNGT ??="y"
CONFIG_SAH_LIB_USERMNGT_USERS_COUNT ??= "16"
CONFIG_SAH_LIB_USERMNGT_GROUPS_COUNT ??= "16"
CONFIG_SAH_LIB_USERMNGT_PASSWD_FILE ??= "/var/run/pcb/pcbpasswd"
CONFIG_SAH_LIB_USERMNGT_HASHING_TOOL ??= "n"

SAH_CONFIG += " \ 
            CONFIG_SAH_LIB_USERMNGT_USERS_COUNT \
            CONFIG_SAH_LIB_USERMNGT_GROUPS_COUNT \
            CONFIG_SAH_LIB_USERMNGT_PASSWD_FILE \
            CONFIG_SAH_LIB_USERMNGT \
            CONFIG_SAH_LIB_USERMNGT_HASHING_TOOL \
            "
