SUMMARY = "This application is the main binary used for creating PCB plug-ins. It provides all the management for an application to connect to a PCB system bus and interact with the other PCB plug-ins."
DESCRIPTION = "pcb_app is a shell application that requires several command line options for it to do something useful.  It connects to a PCB system bus, sets up the logging facilities, loads the plug-in's data model from an ODL file and loads the shared object module that provides the plug-in's actual functionality."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

DEPENDS += "libpcb libmtk"

CONFIG_SAH_SERVICES_PCB-APP ??= "y"

SAH_CONFIG += " \
                CONFIG_SAH_SERVICES_PCB-APP \
                "
