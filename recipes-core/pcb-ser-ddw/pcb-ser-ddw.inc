SUMMARY = "This library provides the code to serialize and deserialize the requests going over a PCB connection into the DDW format."
DESCRIPTION = "A PCB plug-in creates requests in memory. When passed over the PCB bus, these requests first need to be serialized to a commonly known format, independent of word size or endianess.  The receiver of the request need to deserialize it first, before it can process the request and send a reply.  The DDW serializer is used as main serializer for communication between PCB plug-in's and the PCB system bus."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

DEPENDS += "libpcb libusermngt"

CONFIG_SAH_SERVICES_PCB_SER_DDW ??= "y"

SAH_CONFIG += "CONFIG_SAH_SERVICES_PCB_SER_DDW \
"

pkg_postinst_${PN} () {
    echo "/usr/lib/pcb" >> $D/etc/ld.so.conf
}
