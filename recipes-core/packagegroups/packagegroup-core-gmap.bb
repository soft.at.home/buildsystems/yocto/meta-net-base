SUMMARY = "package group with sah gmap packages"

DESCRIPTION = "package group with sah gmap packages"

LICENSE = "MIT"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES = "${PN}"

RDEPENDS_${PN} = " \
    pcb-app \
    pcb-bus \
    pcb-cli \
    pcb-ser-ddw \
    pcb-ser-odl \
    nemo \
    netdev \
    gmap \
    gmap-self \
    gmap-hgw \
    gmap-ethernet-devices \
    "
