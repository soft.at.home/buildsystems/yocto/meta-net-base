SUMMARY = "The module libgmap provides functions to manipulate the gMap data model.  gMap modules must use this library to change or manipulate the gMap data model."
DESCRIPTION = "The module libgmap provides functions to manipulate the gMap data model.  gMap modules must use this library to change or manipulate the gMap data model."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_LIB_GMAP ??= "y"
CONFIG_SAH_LIB_GMAP_DUP_NAME ??= "y"
CONFIG_SAH_LIB_GMAP_SEND_SUBOBJECT_EVENT ??= ""
CONFIG_SAH_LIB_GMAP_ADDED_DELETED_SEND_EVENT ??= ""
CONFIG_SAH_LIB_GMAP_SEND_NO_EVENT ??= ""


SAH_CONFIG += "CONFIG_SAH_LIB_GMAP \
               CONFIG_SAH_LIB_GMAP_DUP_NAME \
               CONFIG_SAH_LIB_GMAP_SEND_SUBOBJECT_EVENT \
               CONFIG_SAH_LIB_GMAP_ADDED_DELETED_SEND_EVENT \
               CONFIG_SAH_LIB_GMAP_SEND_NO_EVENT \
               "

DEPENDS += "libsahtrace libpcb bison-native"
