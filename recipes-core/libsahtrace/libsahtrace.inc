SUMMARY = "Small and flexible library to enable tracing and logging"
DESCRIPTION = "This library is created to provide a simple and flexible system wide tracing and debuging functionality.  The library has some compile time options: * Disable log and tracing completely.  * Configure a default trace level * Use colors in trace messages The library also has runtime options to configure tracelevels dynamically.  The library supports tracezones to group log statements.  "
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base

CONFIG_SAH_TRACES_ENABLED ??= "y"
CONFIG_SAH_TRACES_LEVEL ??= "500"
CONFIG_SAH_TRACES_SYSLOG_CONSOLE ??= "y"
CONFIG_SAH_TRACE_NO_COLORS ??= "y"

SAH_CONFIG+="\
            CONFIG_SAH_TRACES_ENABLED \
            CONFIG_SAH_TRACES_LEVEL \
            CONFIG_SAH_TRACES_SYSLOG_CONSOLE \
            CONFIG_SAH_TRACE_NO_COLORS \
            "
