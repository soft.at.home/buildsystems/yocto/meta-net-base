SUMMARY = "A library to allow shared object modules to be loaded dynamically into a running PCB plug-in."
DESCRIPTION = "An MTK (Modularisation ToolKit) module is a shared object that can be loaded and unloaded, enabled or disabled dynamically into a running PCB plug-in process.  It gains access to the plug-in's data model and can call internal functions.  It allows to extend the functionality of existing plug-ins. All regular PCB plug-ins, started with the pcb_app binary, support loading MTK modules."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

DEPENDS += "libpcb libsahtrace"

CONFIG_SAH_LIB_MTK ??= "y"

SAH_CONFIG += " \
                CONFIG_SAH_LIB_MTK \
                "
