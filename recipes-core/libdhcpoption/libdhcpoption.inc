SUMMARY = "Library to parse and evaluate the binary data of DHCP options."
DESCRIPTION = "The DHCP protocol allows to send and receive DHCP options.  These provide important additional data regarding the network settings of where the host is connected to.  The format of the DHCP options is depending on the option identifier.  The value is usually provided in a binary format that requires parsing.  This library provides an API to parse the binary data of the DHCP option values.  It returns the same data in a variant_t data format."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig 

CONFIG_SAH_LIB_DHCPOPTION ??= "y"

SAH_CONFIG += "CONFIG_SAH_LIB_DHCPOPTION \ 
               " 

DEPENDS += "libsahtrace libpcb" 
