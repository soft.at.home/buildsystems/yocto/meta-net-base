SUMMARY = "Library providing common functionality to the PCB ecosystem.  The functionality includes variants management, connections management, management of the own data model and functions to execute requests and RPC's to other PCB plug-in's.  "
DESCRIPTION = "This software provides the following functionality sets to PCB applications: * Utils : API to manage * Variants, strings, linked lists, ...  * Connections, child processes, ...  * Core : API to manage the data model, sending and receiving requests, ...  Each set is built into its own shared object file.  "
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

TARGET_CC_ARCH += "${LDFLAGS}"

CONFIG_SAH_LIB_PCB ??= "y"
CONFIG_PCB_UTILS_ONLY ??= "n"
CONFIG_PCB_OPEN_SSL_SUPPORT ??= "y"
CONFIG_PCB_ALLOW_ANONYMOUS_TCP ??= "n"
CONFIG_PCB_IGNORE_ACL ??= "n"
CONFIG_PCB_ALWAYS_ACCEPT_IPC ??= "n"
CONFIG_PCB_HELP_SUPPORT ??= "y"
CONFIG_PCB_DEFAULT_PLUGIN_PATH ??= "/usr/lib/pcb"
CONFIG_PCB_ACL_PASSWD ??= "n"
CONFIG_PCB_ACL_USERMNGT ??= "y"
CONFIG_SAH_LIB_PCB_CAPABILITIES ??= "n"
CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME ??=  "${@"pcbapp" if (d.getVar("CONFIG_SAH_LIB_PCB_CAPABILITIES") != "n") else "n"}"
CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME ??= "${@"pcbapp" if (d.getVar("CONFIG_SAH_LIB_PCB_CAPABILITIES") != "n") else "n"}"

SAH_CONFIG += " \
                CONFIG_SAH_LIB_PCB \
                CONFIG_PCB_UTILS_ONLY \
                CONFIG_PCB_OPEN_SSL_SUPPORT \
                CONFIG_PCB_ALLOW_ANONYMOUS_TCP \
                CONFIG_PCB_IGNORE_ACL \
                CONFIG_PCB_ALWAYS_ACCEPT_IPC \
                CONFIG_PCB_HELP_SUPPORT \
                CONFIG_PCB_DEFAULT_PLUGIN_PATH \
                CONFIG_PCB_ACL_PASSWD \
                CONFIG_PCB_ACL_USERMNGT \
                CONFIG_SAH_LIB_PCB_CAPABILITIES \
                CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_USERNAME \
                CONFIG_SAH_LIB_PCB_PRIV_DROP_DEFAULT_GROUPNAME \
                "

DEPENDS += "libsahtrace openssl openssl-native"
DEPENDS += "${@ "libusermngt" if (d.getVar("CONFIG_PCB_ACL_USERMNGT") is "y") else ""}"
