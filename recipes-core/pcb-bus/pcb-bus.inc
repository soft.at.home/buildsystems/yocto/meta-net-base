SUMMARY = "This application acts as an PCB system bus.  It connects all PCB plug-ins connected to the bus and forwards requests between them."
DESCRIPTION = "The PCB system bus is the application that all other PCB plug-ins from the same PCB ecosystem connect to.  The system bus will allow communication between the different plug-ins and forward requests and their replies towards each other.  It also provides its own data model with information about each connected plug-in, some debugging tools and the possibility to connect busses to each other."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig sah_initscripts

INITSCRIPT_PARAM="sysbus:${CONFIG_PCB_SYSBUS_LVL}"

DEPENDS += "libpcb libusermngt"

CONFIG_SAH_SERVICES_PCB-BUS ??= "y"
CONFIG_PCB_SYSBUS_TCP ??= "n"
CONFIG_PCB_SYSBUS_TCP_PORT ??= "${@"7000" if (d.getVar("CONFIG_PCB_SYSBUS_TCP")) else "n"}" 
CONFIG_PCB_SYSBUS_NAME ??= "sysbus"
CONFIG_PCB_SYSBUS_LVL ??= "01"
CONFIG_PCB_SYSBUS_LXC_SUPPORT ??= "n"
CONFIG_PCB_SYSBUS_APPARMOR_SUPPORT ??= "n"

SAH_CONFIG += "\
                CONFIG_PCB_SYSBUS_TCP \
                CONFIG_PCB_SYSBUS_TCP_PORT \
                CONFIG_PCB_SYSBUS_NAME \
                CONFIG_PCB_SYSBUS_LVL \
                CONFIG_PCB_SYSBUS_LXC_SUPPORT \
                CONFIG_PCB_SYSBUS_APPARMOR_SUPPORT \
                CONFIG_SAH_SERVICES_PCB-BUS \
                "

DEPENDS += "${@ "lxc" if (d.getVar("CONFIG_PCB_SYSBUS_LXC_SUPPORT") is "y") else ""}"
DEPENDS += "${@ "apparmor" if (d.getVar("CONFIG_PCB_SYSBUS_APPARMOR_SUPPORT") is "y") else ""}"
FILES_${PN} += "  /usr/lib/pcb_bus/pcb_bus.odl"
