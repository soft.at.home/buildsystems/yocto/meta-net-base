SUMMARY = "gMap server plugin provides a data model containing all discovered devices and information about these devices."
DESCRIPTION = "The basic data model as provided by the gMap server contains a few objects, a basic set of parameters and functions.  Using MIBs the set of parameters and functions can be extended.  The main objects in the gMap data model are: Devices object: this is the root object of the gMap data model. This object does only contain functions (no parameters are available). The functions available make it possible to find specific devices, link devices together, open and close queries. A few generic purpose functions related to managing devices, creating backup and restore files, are present.  Devices.Config object: Each module can add configuration objects and parameters that are specific for the module. Using the config object, a module can provide run-time configuration functionality. Which configuration options are made available is depending on each module.  The gMap core library provides a few global configuration options that are either used by the gMap server or core library itself or used by multiple modules.  Devices.Device.{i} object: Each discovered device is stored in an instance of this multi-instance object. This object provides a basic set of parameters and functions. The instances can be extended using MIBs.  Devices.Query.{i} object: All expressions that need permanent evaluation are stored in an instance of the multi-instance object. gMap modules and gMap clients can open or close queries using a function provided in the Devices object. The gMap core sends events each time a device starts or stops matching an expression stored in these query objects."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_SERVICES_GMAP ??= "y"
CONFIG_SAH_SERVICES_GMAP_MOD_MAIN ??= "n"
CONFIG_SAH_SERVICES_GMAP_NETWORK_INTERFACES ??= "lan,guest"
CONFIG_SAH_SERVICES_GMAP_MAX_LAN_DEVICES ??= "0"
CONFIG_SAH_SERVICES_GMAP_MAX_LAN_DEVICES_ACCEPT ??= "y"
CONFIG_SAH_SERVICES_GMAP_MAX_LAN_DEVICES_DELETE_ACTIVE ??= "n"
CONFIG_SAH_SERVICES_GMAP_MAX_INACTIVE_AGE ??= "30"
CONFIG_SAH_SERVICES_GMAP_DEVICETYPE_ORDER ??= "webui,user,dil,tags,mdns,dhcp,lltd,plc,mac,upnp,usb,voice,default"
CONFIG_SAH_SERVICES_GMAP_NAME_ORDER ??= "webui,user,dil,tags,mdns,dhcp,lltd,plc,mac,upnp,usb,voice,default"
CONFIG_SAH_SERVICES_GMAP_KEEP_LINK ??= "n"
CONFIG_SAH_SERVICES_GMAP_ACCEPT_INTFS ??= "tap0"
CONFIG_SAH_SERVICES_GMAP_HGWCFG_ENABLE ??= "n"


SAH_CONFIG += "CONFIG_SAH_SERVICES_GMAP \
               CONFIG_SAH_SERVICES_GMAP_MOD_MAIN \
               CONFIG_SAH_SERVICES_GMAP_NETWORK_INTERFACES \
               CONFIG_SAH_SERVICES_GMAP_MAX_LAN_DEVICES \
               CONFIG_SAH_SERVICES_GMAP_MAX_LAN_DEVICES_ACCEPT \
               CONFIG_SAH_SERVICES_GMAP_MAX_LAN_DEVICES_DELETE_ACTIVE \
               CONFIG_SAH_SERVICES_GMAP_MAX_INACTIVE_AGE \
               CONFIG_SAH_SERVICES_GMAP_DEVICETYPE_ORDER \
               CONFIG_SAH_SERVICES_GMAP_NAME_ORDER \
               CONFIG_SAH_SERVICES_GMAP_KEEP_LINK \
               CONFIG_SAH_SERVICES_GMAP_ACCEPT_INTFS \
               CONFIG_SAH_SERVICES_GMAP_HGWCFG_ENABLE \
               "

DEPENDS += "libsahtrace libpcb libmtk libnemo-client libgmap libgmap-extensions"
DEPENDS += "${@ "libhgwcfg" if (d.getVar("CONFIG_SAH_SERVICES_GMAP_HGWCFG_ENABLE")=="y") else ""}"


inherit sah_initscripts
INITSCRIPT_PARAM += "gmap:71:10"

inherit sah_backuprestore
BACKUPRESTORE_NAME="${@ "gmap" if (d.getVar("CONFIG_SAH_SERVICES_GMAP_HGWCFG_ENABLE")=="y") else ""}"
BACKUPRESTORE_LEVEL="10"

FILES_${PN} += "/usr/lib/gmap/gmap.so* \
                /usr/lib/gmap/config/*.odl \
                /usr/lib/gmap/mibs/*.odl \
                /usr/lib/gmap/mibs/impl/*.so \
                /usr/lib/gmap/module-scripts \
                /usr/lib/processmonitor/scripts/gmap"
