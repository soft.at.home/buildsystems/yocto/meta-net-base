SUMMARY = "NetDev monitors the operating system's network devices."
DESCRIPTION = "NetDev monitors the operating system's network devices.  It has a direct link with NeMo to report any updates.  NeMo can push updates done in its own data model through NetDev to the kernel.  "
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_SERVICES_NETDEV ??= "y"
CONFIG_SAH_SERVICES_NETDEV_WRITE_LINK_WITH_IOCTL ??= "n"
SAH_SERVICES_NETDEV_64_BITS_NETWORK_STATISTICS ??= "y"


SAH_CONFIG += "CONFIG_SAH_SERVICES_NETDEV \
               CONFIG_SAH_SERVICES_NETDEV_WRITE_LINK_WITH_IOCTL \
               SAH_SERVICES_NETDEV_64_BITS_NETWORK_STATISTICS \
               "

DEPENDS += "libsahtrace libpcb libnemo libmtk"

inherit sah_initscripts
INITSCRIPT_PARAM="netdev:21:"

FILES_${PN} += "/usr/lib/nemo/modules/*.so /usr/lib/netdev/*.so /usr/lib/netdev/*.sh"
