SUMMARY = "Library providing the client interface towards the NeMo plug-in. It's a wrapper around the NeMo RPC's for ease of use."
DESCRIPTION = "The NeMo plug-in provides the NeMo data model, exporting all known interfaces. The plug-in has several RPC's to manage these interfaces, to link or unlink them to each other and to open queries. This library is a wrapper around this functionality to be used by other plug-in's or clients that want to interact with NeMo. It provides a synchronous C API and allows installing callback functions upon an update of a query result."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_LIB_NEMO_CLIENT ??= "y"


SAH_CONFIG += "CONFIG_SAH_LIB_NEMO_CLIENT \
               "

DEPENDS += "libsahtrace libpcb"
