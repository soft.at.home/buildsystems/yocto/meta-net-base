SUMMARY = "This component is a container for shared object modules to extend the NeMo functionality, and for MIBs to extend its data model interfaces."
DESCRIPTION = "This component contains code for shared object modules that can be loaded in either the `nemo-core` or the `nemo-clients` processes.  The modules for the core will extend the core's functionality directly.  They have full access to the data model and already existing internal API.  Generally, they are accompied by a MIB, an extension to the base NeMo data model.  The modules for the clients need to use the API as exported by `lib_nemo-clients`.  They do not have direct access to the data model, but have to use the RPC's as provided by the NeMo core.  "
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_SERVICES_NEMO ??= "y"
CONFIG_SAH_SERVICES_NEMO_DSL_POLL_INTERVAL ??= "10000"
CONFIG_SAH_SERVICES_NEMO_VPN ??= "y"
CONFIG_SAH_SERVICES_NEMO_ETHTOOL_SUPPORT ??= "y"
CONFIG_SAH_SERVICES_NEMO_SFP ??= "y"
CONFIG_SAH_SERVICES_NEMO_SFP_POLL_INTERVAL ??= "2000"
CONFIG_SAH_SERVICES_NEMO_INSTALL_GPON_MIB ??= "y"
CONFIG_SAH_SERVICES_NEMO_CGNAT ??= "y"


SAH_CONFIG += "CONFIG_SAH_SERVICES_NEMO \
               CONFIG_SAH_SERVICES_NEMO_DSL_POLL_INTERVAL \
               CONFIG_SAH_SERVICES_NEMO_VPN \
               CONFIG_SAH_SERVICES_NEMO_ETHTOOL_SUPPORT \
               CONFIG_SAH_SERVICES_NEMO_SFP \
               CONFIG_SAH_SERVICES_NEMO_SFP_POLL_INTERVAL \
               CONFIG_SAH_SERVICES_NEMO_INSTALL_GPON_MIB \
               CONFIG_SAH_SERVICES_NEMO_CGNAT \
               "

DEPENDS += "libsahtrace libpcb libnemo-client libnemo libdhcpoption libmtk"

FILES_${PN} += "/usr/lib/nemo/modules/*.so /usr/lib/nemo/clients/*.so /usr/lib/nemo/mibs/*.odl"
