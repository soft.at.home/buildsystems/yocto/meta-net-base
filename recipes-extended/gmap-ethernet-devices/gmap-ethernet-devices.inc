SUMMARY = "The gmap-ethernet-devices client module sets the ip information of discovered network devices in the gmap data model and link discovered devices objects to the right device interface object of the box."
DESCRIPTION = "This module relies on different sources to discover the ip addresses: * netdev data model Linking to the right box device interface relies on netlink neighbor events.  When a new device is discovered via netlink events the following tags are set `lan edev mac physical eth`.  Finally, attached mibs are loaded and parameters updated.  When ip addresses are discovered, ipv4 and ipv6 tags will be additionally set on the device, ip mib loaded, and ip mib parameters updated including status (reachable or not).  The selection of the IP address for the device is done by the ip mib shared library. This library will also manage the Active state of the device depending if at least one ip address is reachable.  To verify an (ip, mac) is still valid the gmap-ethernet-devices  module relies on the gmap arping submodule of the libgmap-extensions library."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_SERVICES_GMAP_ETHERNET_DEV ??= "y"
CONFIG_SAH_SERVICES_GMAP_STP ??= "y"


SAH_CONFIG += "CONFIG_SAH_SERVICES_GMAP_ETHERNET_DEV=${CONFIG_SAH_SERVICES_GMAP_ETHERNET_DEV} \
               CONFIG_SAH_SERVICES_GMAP_STP=${CONFIG_SAH_SERVICES_GMAP_STP} \
               "

DEPENDS += "libsahtrace libpcb libmtk libgmap libgmap-extensions libmnl libdhcpoption"

# Skip QA step for package as '-dev' suffix of package name causes unintended errors
INSANE_SKIP_${PN} = "dev-elf"

FILES_${PN} += "/usr/lib/gmap/modules/*.so \
                /usr/lib/gmap/module-scripts/*.sh"
