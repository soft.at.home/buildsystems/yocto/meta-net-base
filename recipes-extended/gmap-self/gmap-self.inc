SUMMARY = "The box device itself and its network interfaces are key part of the gmap datamodel. They are managed by the gmap-self client module."
DESCRIPTION = "At startup, the gmap-self client module creates the device box object identified by its lan bridge mac address, and sets the "self physical" tags on it.  This module is also in charge to retrieve and build the network interface device objects topology of the box: * it will create the bridge device object(s), link them to the box device , mark them with the (self lan mac nemo interface) tags, update information of the attached mibs including the ip address (ipv4 and ipv6 tags are set), and manage their Active state.  * it will create the ethernet interface device objects, link them to their upper bridge device, mark them with the (self lan eth nemo interface) tags, update information of the attached mibs, and manage their Active state."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_SERVICES_GMAP_SELF ??= "y"
CONFIG_SAH_SERVICES_GMAP_MOD_SELF_IS_HGW ??= "y"
CONFIG_SAH_SERVICES_GMAP_MOD_SELF_USE_OLD_HGW_OBJECT ??= "n"
CONFIG_SAH_SERVICES_GMAP_MOD_SELF_BLOCKED_DEVICES ??= ""


SAH_CONFIG += "CONFIG_SAH_SERVICES_GMAP_SELF=${CONFIG_SAH_SERVICES_GMAP_SELF} \
               CONFIG_SAH_SERVICES_GMAP_MOD_SELF_IS_HGW=${CONFIG_SAH_SERVICES_GMAP_MOD_SELF_IS_HGW} \
               CONFIG_SAH_SERVICES_GMAP_MOD_SELF_USE_OLD_HGW_OBJECT=${CONFIG_SAH_SERVICES_GMAP_MOD_SELF_USE_OLD_HGW_OBJECT} \
               CONFIG_SAH_SERVICES_GMAP_MOD_SELF_BLOCKED_DEVICES=${CONFIG_SAH_SERVICES_GMAP_MOD_SELF_BLOCKED_DEVICES} \
               "

DEPENDS += "libsahtrace libpcb libmtk libnemo-client libgmap libgmap-extensions"

FILES_${PN} += "/usr/lib/gmap/modules/*.so \
                /usr/lib/gmap/config/*.odl \
                /usr/lib/gmap/module-scripts/*.sh"
