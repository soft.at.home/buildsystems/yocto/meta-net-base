SUMMARY = "The module libgmap-extensions provides a set of functions that are common for different modules, but are not part of the gMap core."
DESCRIPTION = "The module libgmap-extensions provides a set of functions that are common for different modules, but are not part of the gMap core.  Instead of duplicating the same functionality in different modules, the shared functionality can be put in the extension library.  Not all modules have to use the extension library."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_LIB_GMAP_EXTENSIONS ??= "y"


SAH_CONFIG += "CONFIG_SAH_LIB_GMAP_EXTENSIONS \
               "

DEPENDS += "libsahtrace libpcb libmtk libgmap libusermngt"
