SUMMARY = "The gmap-hgw client module manages the "HGW" device object of the gmap datamodel."
DESCRIPTION = "The "HGW" device object is a device alternative object.  The device master object it refers depends on the type of the box gmap is running on: When it is a HGW, the `HGW` device master object is the box device object itself (identified by its mac address)"
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

CONFIG_SAH_SERVICES_GMAP_HGW ??= "y"
CONFIG_SAH_SERVICES_GMAP_HGW_DEVICE ??= "y"


SAH_CONFIG += "CONFIG_SAH_SERVICES_GMAP_HGW=${CONFIG_SAH_SERVICES_GMAP_HGW} \
               CONFIG_SAH_SERVICES_GMAP_HGW_DEVICE=${CONFIG_SAH_SERVICES_GMAP_HGW_DEVICE} \
               "

DEPENDS += "libsahtrace libpcb libmtk libgmap"

FILES_${PN} += "/usr/lib/gmap/modules/*.so \
                /usr/lib/gmap/module-scripts/*.sh"
