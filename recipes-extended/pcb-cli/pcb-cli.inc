SUMMARY = "This application provides a CLI (Command Line Interface) towards the PCB ecosystem it is connected to."
DESCRIPTION = "The application pcb_cli can connect to a PCB system bus.  Using the CLI, it is possible to inspect each plug-in's data model, install update notifications and execute the RPC's exported by the plug-in's."
LICENSE = "BSD-plus-patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b144ccd4a02477481c61bc89602d4400"

inherit sah_base pkgconfig

DEPENDS += "libpcb libusermngt"

CONFIG_SAH_SERVICES_PCB_CLI_USER_CHECK ??= "y"
CONFIG_SAH_SERVICES_PCB-CLI ??= "y"


SAH_CONFIG += "CONFIG_SAH_SERVICES_PCB_CLI_USER_CHECK \
CONFIG_SAH_SERVICES_PCB-CLI"
