# meta-net-base

Sop layer containing the net base components.


## Adding the meta-net-base layer to your build

```bash
bitbake-layers add-layer meta-net-base
```

## Layer content

### Recipes
- gmap
- gmap-ethernet-devices
- gmap-hgw
- gmap-self
- libdhcpoption
- libgmap
- libgmap-extensions
- libmtk
- libnemo
- libnemo-client
- libpcb
- libsahtrace
- libusermngt
- nemo
- netdev
- pcb-app
- pcb-bus
- pcb-cli
- pcb-ser-ddw
- pcb-ser-odl


### bbappends

### Packagegroups
- packagegroup-core-gmap

